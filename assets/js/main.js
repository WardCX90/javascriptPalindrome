var userInput = document.getElementById("userInput");
function displayText(){
	var userInput = document.getElementById("userInputField").value;
	var userInput = userInput.toUpperCase();
	var splitInput = userInput.split("");
	var reverseInput = splitInput.reverse();
	var rejoinInput = reverseInput.join("");
	function checker(){
		  if (userInput == rejoinInput){
		  		document.getElementById("userInput").innerHTML = "<span class='input-style'>" + userInput + "</span>" + " in reverse is " + 
		  		"<span class='input-reverse'>" + rejoinInput + "</span>" + " and therefore, it " + "<span class='input-true'>" + "is" + "</span>" + 
		  		" a Palindrome.";
		  } else {
		      	document.getElementById("userInput").innerHTML = "<span class='input-style'>" + userInput + "</span>" + " in reverse is " + 
		  		"<span class='input-reverse'>" + rejoinInput + "</span>" + " and therefore, it " + "<span class='input-false'>" + "is&nbsp;NOT" + "</span>" + 
		  		" a Palindrome.";
		  }
	}
	checker();
}